const mongoose = require("mongoose");




const userSchema = new mongoose.Schema({

    firstName: {
        // Data type
        type: String,
        required: [true, "First name is required"] //To block invalid name 
        // required:[true(statement),false(statement)]
    },
    lastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    email: {
        type: String,
        required: [true, "email is required"]
    },
    password: {
        type: String,
        required: [true, "password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    mobileNo: {
        type: String,
        required: [true, "Mobile number is required"]
    },
    enrollments: [
        {
            courseId: {
                type: String,
                required: [true, "Course ID is required"]
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            },
            status: {
                type: String,
                default: "Enrolled"
            }
        },

    ]
})

module.exports = mongoose.model("User", userSchema)