const express = require("express");
const router = express.Router();


// Import
const userController = require("../controllers/userController")
const auth = require("../auth");
// Check Email
router.post("/checkEmail", (req, res) => {

    userController.checkEmailExists(req.body).then(result => { res.send(result) });

});

// Route for registration
router.post("/register", (req, res) => {

    userController.registerUser(req.body).then(result => { res.send(result) })

})

// Route for user authentication

router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(result => {
        res.send(result)
    })

})


// Route for enrolling an authenticated user

router.post("/enroll", auth.verify, (req, res) => {
    let data = {
        // user ID will be retrieved from the request header
        userId: auth.decode(req.headers.authorization).id,

        // Course ID will be retrieve from the request body
        courseId: req.body.courseId
    }
    userController.enroll(data).then(result => res.send(result));

})



// [Activity]

// router.get("/details", (req, res) => {

//     userController.userDetails(req.body).then(result => { res.send(result) })

// })
// Route for retrieving user details
router.post("/details", (req, res) => {

    // Provides the user's ID for the getProfile controller method
    userController.getProfile({ userId: req.body.id }).then(resultFromController => res.send(resultFromController));

});






module.exports = router;